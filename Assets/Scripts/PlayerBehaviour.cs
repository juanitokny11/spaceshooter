﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

    private Vector2 axis;
    public float speed;
    public float limitx;
    public float limity;
    private Vector3 tmpPos; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(axis * speed * Time.deltaTime);

        if(transform.position.x > limitx)
        {
            tmpPos = new Vector3(limitx, transform.position.y, transform.position.z);
            transform.position = tmpPos;

        }else if(transform.position.x < -limitx)
        {
            tmpPos = new Vector3(-limitx, transform.position.y, transform.position.z);
            transform.position = tmpPos;
        }
        if(transform.position.y > limity)
        {
            tmpPos = new Vector3( transform.position.x, limity, transform.position.z);
            transform.position = tmpPos;
        } else if(transform.position.y < -limity) {
            tmpPos = new Vector3(transform.position.x, -limity, transform.position.z);
            transform.position = tmpPos;
        }

    }
    public void SetAxis(Vector2 currentAxis) {
        axis = currentAxis;
    }
}
